﻿using BattleShips.ConsoleChecker;
using BattleShips.Setup;
using BattleShips.BShip;
using NUnit.Framework;

namespace BattleShipsTests
{
    [TestFixture]
    public class GameSetupTests
    {
        [Test]
        public void CheckGameSetup_WithDefaults_SetsDefaults()
        {
            // arrange
            var thisGame = new GameSetup(new MockSetupDefault());
            var actual = "";

            // Act
            thisGame.SetupGame();
            // format the actual result
            thisGame.ListOfShipTypes.ForEach(s => actual = actual
                += string.Format("Ship type is {0} and quantity is {1}\n", s.ShipType, s.TypeQuantity));

            //assert
            Assert.AreEqual
                (
                "Ship type is Destroyer and quantity is 1\n"+
                "Ship type is Scout and quantity is 1\n", 
                actual
                );
            Assert.AreEqual(3, thisGame.NumberOfPlayers, "Error: The number of players should be 3.");
            Assert.AreEqual(6, thisGame.GameSea.SeaRow, "Error: The sea row dimension should be 6.");
            Assert.AreEqual(7, thisGame.GameSea.SeaColumn, "Error: The sea column dimension should be 7.");
        }

        [Test]
        public void CheckGameSetup_WithCustom_SetsCustom()
        {
            // arrange
            var thisGame = new GameSetup(new MockSetupCustom());
            var actual = "";

            // act
            thisGame.SetupGame();
            // format the actual result
            thisGame.ListOfShipTypes.ForEach(s => actual
                += string.Format("Ship type is {0} and quantity is {1}\n", s.ShipType, s.TypeQuantity));

            //assert
            Assert.AreEqual
                (
                "Ship type is Scout and quantity is 4\n" +
                "Ship type is Destroyer and quantity is 4\n" +
                "Ship type is Battleship and quantity is 4\n" +
                "Ship type is Submarine and quantity is 4\n", 
                actual
                );
            Assert.AreEqual(5, thisGame.NumberOfPlayers, "Error: The number of players should be 5.");
            Assert.AreEqual(10, thisGame.GameSea.SeaRow, "Error: The sea row dimension should be 10.");
            Assert.AreEqual(10, thisGame.GameSea.SeaColumn, "Error: The sea column dimension should be 10.");
        }
    }
}