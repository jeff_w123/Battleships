﻿using BattleShips;
using BattleShips.BattleGround;
using BattleShips.BShip;
using System.Collections.Generic;
namespace BattleShipsTests.Helpers
{
    public static class BattleshipsHelper
    {
        /// <summary>
        /// Creates a ship of type, orientation, start position
        /// and optional default of floating.
        /// </summary>

        public static Ship CreateShip(
            ShipType thisType, 
            bool horizontally, 
            Position startPosition, 
            bool isAfloat = true)
        {
            var thisShip = new Ship(thisType, horizontally);
            thisShip.SetShipPositions(startPosition);
            if (!isAfloat)
            {
                thisShip.ShipPositions.ForEach(p => p.IsFloating = false);
            }
            return thisShip;
        }

        public static Player CreatePlayer(List<Ship> thisList)
        {
            var thisPlayer = new Player();
            thisPlayer.PlayerShips = thisList;
            return thisPlayer;
        }
    }
}
