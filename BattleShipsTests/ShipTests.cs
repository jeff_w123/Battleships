﻿using BattleShips.BattleGround;
using BattleShips.BShip;
using NUnit.Framework;

namespace BattleShipsTest
{
    [TestFixture]
    public class ShipTests
    {
        [Test]
        public void GetShipPositions_WithBattleship_Returns3Positions()
        {
            // arrange
            var placePosition = new Position(2, 3);
            var thisShip = new Ship(ShipType.Battleship, true);

            // act
            var actualResult = thisShip.GetShipPositions(placePosition);

            // assert
            Assert.AreEqual(3, actualResult.Count);
        }

        [Test]
        public void GetShipPositions_WithDestroyer_ReturnsHorizontalPositions()
        {
            // arrange
            var placePosition = new Position(2, 3);
            var thisShip = new Ship(ShipType.Destroyer, true);
            var actual = "";

            // act
            var ThisList = thisShip.GetShipPositions(placePosition);
            ThisList.ForEach(p => actual += string.Format("{0},{1},{2}\n", p.IsFloating, p.Row, p.Column));

            // assert
            Assert.AreEqual("False,2,3\nFalse,3,3\n", actual);
        }

        [Test]
        public void GetShipPositions_WithBattleship_ReturnsVerticalPositions()
        {
            // arrange
            var placePosition = new Position(2, 3);
            var thisShip = new Ship(ShipType.Battleship, false);
            var actual = "";

            // act
            var ThisList = thisShip.GetShipPositions(placePosition);
            ThisList.ForEach(p => actual += string.Format("{0},{1},{2}\n", p.IsFloating, p.Row, p.Column));

            // assert
            Assert.AreEqual("False,2,3\nFalse,2,4\nFalse,2,5\n", actual);
        }

        [Test]
        public void SetShipPositions_WithBattleship_IsFloatingShouldBeTrue()
        {
            // arrange
            var placePosition = new Position(2, 3);
            var thisShip = new Ship(ShipType.Battleship, true);

            // act
            thisShip.SetShipPositions(placePosition);
            var floatingPositionsAfter = thisShip.ShipPositions.TrueForAll(p => p.IsFloating == true);

            // assert
            Assert.AreEqual(true, floatingPositionsAfter);
        }
    }
}