﻿using BattleShips.ConsoleChecker;
using BattleShips.Setup;
using NUnit.Framework;
using System;

namespace BattleShipsTests
{
    [TestFixture]
    public class GameSetupParserTests
    {
        [TestCase("2", 2)] // boundary test case!
        [TestCase("10", 10)]
        public void SetNumberOfPlayers_WithValidInput_ReturnsInt(string input, int expected)
        {
            // arrange
            var inputPlayerTest = new GameSetupParser(new MockConsoleReader(input));

            // act
            var actual = inputPlayerTest.SetNumberOfPlayers();

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("0", 0)] // boundary test case!
        [TestCase("3", 3)]
        public void SetNumberOfShips_WithValidInput_ReturnsInt(string input, int expected)
        {
            // arrange
            var inputPlayerTest = new GameSetupParser(new MockConsoleReader(input));

            // act
            var actual = inputPlayerTest.SetNumberOfShips();

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("0", GameMode.Default)]
        [TestCase("1", GameMode.Custom)]
        public void SetGameMode_WithValidInput_ReturnsEnum(string input, GameMode expected)
        {
            // arrange
            var inputModeTest = new GameSetupParser(new MockConsoleReader(input));

            // act
            var actual = inputModeTest.SelectGameMode();

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("-1")]
        [TestCase("Gus Fring")]
        public void SetGameMode_WithInvalidInput_IsValidInputShouldBeFalse(string input)
        {
            // arrange
            var inputModeTest = new GameSetupParser(new MockConsoleReader(input));

            // act
            inputModeTest.processUserModeInput(Enum.GetValues(typeof(GameMode)).Length);

            // assert
            Assert.IsFalse(inputModeTest.IsValidInput);
        }
    }
}