﻿using BattleShips;
using BattleShips.BattleGround;
using BattleShips.BShip;
using BattleShipsTests.Helpers;
using NUnit.Framework;
using System.Collections.Generic;

namespace BattleShipsTests
{
    [TestFixture]
    public class PlayerSetupTests
    {
        [TestCase(4, 3)]
        [TestCase(11, 11)]
        public void CheckIsPositionAvailable_WithUnusedPositions_ReturnsTrue(int x, int y)
        {
            // arrange
            List<Position> listToCheck = new List<Position>() 
            { 
                new Position(2, 2), 
                new Position(5, 5), 
                new Position(11, 5) 
            };
            var thisPlayerSetup = new PlayerShipValidation();

            // act
            var result = thisPlayerSetup.IsPositionAvailable(new Position(x, y), listToCheck);

            // assert
            Assert.IsTrue(result);
        }

        [TestCase(3, 2)]
        [TestCase(5, 6)]
        public void CheckIsPositionAvailable_WithOccupiedPositions_ReturnsFalse(int x, int y)
        {
            // arrange
            List<Position> listToCheck = new List<Position>() 
            { 
                new Position(3, 2), 
                new Position(5, 6), 
                new Position(11, 5) 
            };
            var thisPlayerSetup = new PlayerShipValidation();

            // act
            var result = thisPlayerSetup.IsPositionAvailable(new Position(x, y), listToCheck);

            // assert
            Assert.IsFalse(result);
        }

        [Test]
        public void GetPlayerShipPositions_WithShipList_ReturnsAllPositions()
        {
            // arrange
            var shipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Submarine, 
                true, 
                new Position(3,3), 
                isAfloat: true),

                BattleshipsHelper.CreateShip(ShipType.Destroyer,
                true, 
                new Position(4,5), 
                isAfloat: true)
            };
            var thisPlayerSetup = new PlayerShipValidation();

            // act
            var result = thisPlayerSetup.GetPlayerShipsPositions(shipList);

            // assert
            Assert.AreEqual(7, result.Count);
        }

        [Test]
        public void CanShipBeAdded_WithValidShipPositions_ReturnsTrue()
        {
            // arrange
            var thisSea = new Sea(10, 10);
            List<Position> occupiedShipPositions = new List<Position>() 
            { 
                new Position(3, 3), 
                new Position(4, 5), 
                new Position(6, 8) 
            };
            List<Position> thisShipsPositions = new List<Position>() 
            { 
                // valid ship positions!!
                new Position(2, 3), 
                new Position(2, 2) 
            };
            var thisPlayerSetup = new PlayerShipValidation();

            // act
            var result = thisPlayerSetup.CanShipBeAdded(thisSea, thisShipsPositions, occupiedShipPositions);

            // assert
            Assert.IsTrue(result);
        }

        [Test]
        public void CanShipBeAdded_WithOutOfSeaBoundsPositions_ReturnsFalse()
        {
            // arrange
            var thisSea = new Sea(10, 10);
            List<Position> occupiedShipPositions = new List<Position>() 
            { 
                new Position(3, 3), 
                new Position(4, 5), 
                new Position(6, 8) 
            };
            List<Position> shipsPosition = new List<Position>() 
            { 
                // One of these positions is outside the sea!!
                new Position(9, 10), 
                new Position(9, 9) 
            };
            var thisPlayerSetup = new PlayerShipValidation();

            // act
            var result = thisPlayerSetup.CanShipBeAdded(thisSea, shipsPosition, occupiedShipPositions);

            // assert
            Assert.IsFalse(result, "Error: Should be false as at least one position is outside the sea.");
        }

        [Test]
        public void CanShipBeAdded_WithPositionAlreadyOccupied_ReturnsFalse()
        {
            // arrange
            var thisSea = new Sea(10, 10);
            List<Position> occupiedShipPositions = new List<Position>() 
            { 
                new Position(3, 3), 
                new Position(4, 5), 
                new Position(6, 8) 
            };
            List<Position> shipsPosition = new List<Position>() 
            { 
                // One of these positions is already occupied!!
                new Position(4, 4), 
                new Position(4, 5) 
            };
            var thisPlayerSetup = new PlayerShipValidation();

            // act
            var result = thisPlayerSetup.CanShipBeAdded(thisSea, shipsPosition, occupiedShipPositions);

            // assert
            Assert.IsFalse(result, "Error: Should be false as at least one position is already occupied");
        }

        [Test]
        public void CheckCreatePlayerShips_WithShipTypeInGameList_ShouldCreateShips()
        {
            // arrange
            var thisValidation = new PlayerShipValidation();
            List<ShipTypeInGame> theseTypes = new List<ShipTypeInGame>() 
            { 
                new ShipTypeInGame(ShipType.Destroyer, 1),
                new ShipTypeInGame(ShipType.Battleship, 2),
                new ShipTypeInGame(ShipType.Scout, 3)
            };
            var thisPlayer = new Player();

            // act
            thisValidation.createPlayerShips(theseTypes, thisPlayer);

            // assert
            Assert.AreEqual(6, thisPlayer.PlayerShips.Count);
        }
    }
}