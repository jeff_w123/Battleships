﻿using BattleShips.BattleGround;
using BattleShips.ConsoleChecker;
using BattleShips.Setup;
using NUnit.Framework;
using System;

namespace BattleShipsTests
{
    [TestFixture]
    public class GamePlayParserTests
    {
        [Test]
        public void GamePlayParser_WithValidInput_FireCommandShouldReturnPosition()
        {
            // arrange
            string inputString = "2,3";

            // act
            var inputFireTest = new GamePlayParser(new MockConsoleReader(inputString));
            var firePosition = inputFireTest.FireCommand();

            // assert
            Assert.AreEqual(2, firePosition.Row);
            Assert.AreEqual(3, firePosition.Column);
        }

        [Test]
        public void GamePlayParser_WithInvalidInput_IsValidInputShouldBeFalse()
        {
            // arrange
            string inputString = "33,one";

            // act
            var inputFireTest = new GamePlayParser(new MockConsoleReader(inputString));

            // assert
            Assert.IsFalse(inputFireTest.IsValidInput);
        }
    }
}