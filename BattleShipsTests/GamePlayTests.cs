﻿using BattleShips;
using BattleShips.BattleGround;
using BattleShips.BShip;
using BattleShipsTests.Helpers;
using NUnit.Framework;
using System.Collections.Generic;

namespace BattleShipsTests
{
    [TestFixture]
    public class GamePlayTests
    {
        [Test]
        public void CheckUpdatePlayerList_With2DeadPlayers_RemovesAllPlayers()
        {
            // arrange

            var sunkShipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Scout, 
                true, 
                new Position(2,3), 
                isAfloat: false)
            };

            // create a new GamePlay instance
            var thisGamePlay = new GamePlay() 
            {
                PlayerList = new List<Player>() 
                { 
                    // create 2 dead players
                    BattleshipsHelper.CreatePlayer(sunkShipList), 
                    BattleshipsHelper.CreatePlayer(sunkShipList) 
                }
            };
            var noOfPlayersBeforeUpdate = thisGamePlay.PlayerList.Count;

            // act
            thisGamePlay.UpdatePlayerList();

            // assert
            Assert.AreEqual(2, noOfPlayersBeforeUpdate);
            Assert.AreEqual(0, thisGamePlay.PlayerList.Count);
        }

        [Test]
        public void CheckUpdatePlayerList_With1AliveAnd1DeadPlayer_RemovesOnlyDeadPlayer()
        {
            // arrange

            var sunkShipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Scout, 
                true, 
                new Position(2,3), 
                isAfloat: false)
            };
            var floatingShipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Destroyer, 
                true, 
                new Position(4,5), 
                isAfloat: true)
            };

            // create a new GamePlay instance
            var thisGamePlay = new GamePlay()
            {
                PlayerList = new List<Player>() 
                { 
                    // create 2 players, one dead and the other alive
                    BattleshipsHelper.CreatePlayer(sunkShipList), 
                    BattleshipsHelper.CreatePlayer(floatingShipList) 
                }
            };
            var noOfPlayersBeforeUpdate = thisGamePlay.PlayerList.Count;

            // act
            thisGamePlay.UpdatePlayerList();

            // assert
            Assert.AreEqual(2, noOfPlayersBeforeUpdate);
            Assert.AreEqual(1, thisGamePlay.PlayerList.Count);
        }

        [Test]
        public void CheckForWinner_With2Players_IsGameOverShouldBeFalse()
        {
            // arrange
            var thisGamePlay = new GamePlay() 
            { 
                PlayerList = new List<Player>()
                {   
                    // create 2 players
                    new Player(),
                    new Player()
                } 
            };

            // act
            thisGamePlay.CheckForWinner();

            // assert
            Assert.IsFalse(thisGamePlay.IsGameOver);
        }

        [Test]
        public void CheckForWinner_With1Player_IsGameOverShouldBeTrue()
        {
            // arrange
            var thisGamePlay = new GamePlay()
            {
                PlayerList = new List<Player>()
                {   
                    // only one player!
                    new Player(),
                }
            };

            // act
            thisGamePlay.CheckForWinner();

            // assert
            Assert.IsTrue(thisGamePlay.IsGameOver);
        }

        [Test]
        public void CheckGetEnemyPlayers_WithPlayerParam_ReturnsOtherPlayers()
        {
            // arrange
            var thisGamePlay = new GamePlay() 
            { 
                PlayerList = new List<Player>()
                {
                    new Player() { PlayerName = "Saul Goodman" },
                    new Player() { PlayerName = "Frank Underwood" },
                    new Player() { PlayerName = "Keyser Soze" }
                }
            };
            var playerToPassAsParam = thisGamePlay.PlayerList.Find(p => p.PlayerName == "Frank Underwood");

            // act
            var resultList = thisGamePlay.GetEnemyPlayers(playerToPassAsParam);

            // assert
            Assert.AreEqual(2, resultList.Count);
            Assert.AreEqual("Saul Goodman", resultList[0].PlayerName);
            Assert.AreEqual("Keyser Soze", resultList[1].PlayerName);
        }
    }
}