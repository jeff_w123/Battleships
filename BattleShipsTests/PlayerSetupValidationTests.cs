﻿using BattleShips.ConsoleChecker;
using NUnit.Framework;

namespace BattleShipsTests
{
    [TestFixture]
        public class PlayerSetupParserTests
        {

        [TestCase("Jeff")]
        [TestCase("Sasha")]
        public void SetPlayerName_WithValidString_ShouldReturnString(string input)
        {
            // arrange
            var inputPlayerTest = new PlayerSetupParser(new MockConsoleReader(input));

            // act
            var actual = inputPlayerTest.SetPlayerName();

            // assert
            Assert.AreEqual(input, actual);
        }

        [TestCase(null)]
        [TestCase(" ")]
        public void ProcessUserNameInput_WithEmptyString_IsValidInputShouldBeFalse(string input)
        {
            // arrange
            var inputPlayerTest = new PlayerSetupParser(new MockConsoleReader(input));

            // act
            inputPlayerTest.ProcessUserNameInput();

            // assert
            Assert.IsFalse(inputPlayerTest.IsValidInput);
        }

        [TestCase("2,3,h")]
        [TestCase("5,6,v")]
        public void SetShip_WithValidString_ShouldReturnString(string input)
        {
            // arrange
            var inputPlayerTest = new PlayerSetupParser(new MockConsoleReader(input));

            // act
            var actual = inputPlayerTest.PlaceNewShip();

            // assert
            Assert.AreEqual(input, actual);
        }

        [TestCase("2,3,horizontal")]
        [TestCase("5,six,v")]
        public void ValidateUserShipInput_WithInvalidString_IsValidInputShouldBeFalse(string input)
        {
            // arrange
            var inputPlayerTest = new PlayerSetupParser(new MockConsoleReader(input));

            // act
            var irrelevantOutput = inputPlayerTest
                .ValidateUserShipInput(inputPlayerTest.ThisReader.ReadConsole());

            // assert
            Assert.IsFalse(inputPlayerTest.IsValidInput);
        }
    }
}