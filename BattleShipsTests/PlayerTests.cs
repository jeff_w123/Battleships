﻿using BattleShips.BattleGround;
using BattleShips.BShip;
using BattleShipsTests.Helpers;
using NUnit.Framework;
using System.Collections.Generic;

namespace BattleShipsTest
{
    [TestFixture]
    public class PlayerTests
    {
        [Test]
        public void CheckIsShipFloating_WithAllFloatingPositions_ReturnsTrue()
        {
            // arrange
            var floatingShip =
                BattleshipsHelper.CreateShip(ShipType.Scout,
                true,
                new Position(2, 3),
                isAfloat: true);

            // act and assert
            Assert.IsTrue(floatingShip.IsShipFloating());
        }

        [Test]
        public void CheckIsShipFloating_WithSomeFloatingPositions_ReturnsTrue()
        {
            // arrange
            var partiallyFloatingShip =
                BattleshipsHelper.CreateShip(ShipType.Destroyer,
                true,
                new Position(4, 3),
                isAfloat: true);
            // sink one of the positions
            partiallyFloatingShip.ShipPositions[0].IsFloating = false;

            // act and assert
            Assert.IsTrue(partiallyFloatingShip.IsShipFloating());
        }

        [Test]
        public void CheckIsShipFloating_WithAllSunkPositions_ReturnsFalse()
        {
            // arrange
            var sunkShip =
                BattleshipsHelper.CreateShip(ShipType.Submarine,
                true,
                new Position(5, 1),
                isAfloat: false);

            // act and assert
            Assert.IsFalse(sunkShip.IsShipFloating());
        }

        [Test]
        public void CheckIsPlayerAlive_WithDeadPlayer_ReturnsFalse()
        {
            // arrange
            var sunkShipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Scout, 
                true, 
                new Position(2,3), 
                isAfloat: false)
            };
            var deadPlayer = BattleshipsHelper.CreatePlayer(sunkShipList);

            // act and assert
            Assert.IsFalse(deadPlayer.IsPlayerAlive());
        }

        [Test]
        public void CheckIsPlayerAlive_WithAlivePlayer_ReturnsTrue()
        {
            // arrange
            var floatingShipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Scout, 
                true, 
                new Position(4,2), 
                isAfloat: true)
            };
            var alivePlayer = BattleshipsHelper.CreatePlayer(floatingShipList);

            // act and assert
            Assert.IsTrue(alivePlayer.IsPlayerAlive());
        }

        [Test]
        public void CheckGetFloatingShips_WithAllFloating_ReturnsAll()
        {
            // arrange
            var floatingShipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Scout, 
                true, 
                new Position(4,2), 
                isAfloat: true),

                BattleshipsHelper.CreateShip(ShipType.Destroyer, 
                false, 
                new Position(2,3), 
                isAfloat: true)
            };
            var thisPlayer = BattleshipsHelper.CreatePlayer(floatingShipList);

            // act
            var actual = thisPlayer.GetFloatingShips();

            // assert
            Assert.AreEqual(2, actual.Count);
        }

        [Test]
        public void CheckGetFloatingShips_With1Floating_Returns1Ship()
        {
            // arrange
            var floatingShipList = new List<Ship>() 
            {
                BattleshipsHelper.CreateShip(ShipType.Scout, 
                true, 
                new Position(4,2), 
                isAfloat: false),

                BattleshipsHelper.CreateShip(ShipType.Destroyer, 
                false, 
                new Position(2,3), 
                isAfloat: true)
            };
            var thisPlayer = BattleshipsHelper.CreatePlayer(floatingShipList);

            // act
            var actual = thisPlayer.GetFloatingShips();

            // assert
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(ShipType.Destroyer, actual[0].ShipType);
        }
    }
}