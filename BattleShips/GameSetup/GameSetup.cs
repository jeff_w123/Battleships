﻿using BattleShips.BattleGround;
using BattleShips.ConsoleChecker;
using BattleShips.BShip;
using System;
using System.Collections.Generic;
using BattleShips.Properties;

namespace BattleShips.Setup
{
    /// <summary>
    /// This class gets and stores all the information needed to start a new game.
    /// </summary>
    public class GameSetup : IGameSetup
    {
        private IGameSetupParser inputParser;

        public GameSetup(IGameSetupParser thisIParser)
        {
            inputParser = thisIParser;
        }

        // The game setup information will be stored here   
        public int NumberOfPlayers { get; private set; }
        public List<ShipTypeInGame> ListOfShipTypes { get; private set; }
        public Sea GameSea { get; private set; }

        // Public method for access to this class
        public void SetupGame()
        {
            // get and set the number of players
            Console.WriteLine(Resources.getNumberOfPlayers);
            NumberOfPlayers = inputParser.SetNumberOfPlayers(); 

            // get and set the game mode
            Console.WriteLine(Resources.getGameMode);
            var gameMode = inputParser.SelectGameMode();
            ListOfShipTypes = GameInput.GetSettings(gameMode, inputParser);

            // get and set the sea dimensions
            Console.WriteLine(Resources.getSeaSize);
            GameSea = inputParser.SetSeaSize();
        }
    }
}
