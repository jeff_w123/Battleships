﻿using System.Collections.Generic;
using BattleShips.BattleGround;
using BattleShips.BShip;

namespace BattleShips.Setup
{
    public interface IGameSetup
    {
        Sea GameSea { get; }
        List<ShipTypeInGame> ListOfShipTypes { get; }
        int NumberOfPlayers { get; }

        void SetupGame();
    }
}