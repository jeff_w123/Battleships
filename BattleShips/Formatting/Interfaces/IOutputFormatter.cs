﻿using System.IO;

namespace BattleShips.Formatting
{
    public interface IOutputFormatter
    {
        TextWriter OutputTextWriter { get; }
    }
}