﻿namespace BattleShips.BShip
{
    /// <summary>
    /// This class is used to store the ship type and quantity used in a game.
    /// </summary>
    public class ShipTypeInGame
    {
        public ShipType ShipType { get; set; }
        public int TypeQuantity { get; set; }

        public ShipTypeInGame(ShipType thisType, int thisQuantity)
        {
            ShipType = thisType;
            TypeQuantity = thisQuantity;
        }
    }
}
