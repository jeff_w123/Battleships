﻿using BattleShips.Properties;
using System;

namespace BattleShips.ConsoleChecker
{
    /// <summary>
    /// This is a list of validation methods used to check user input during player setup.
    /// </summary>
    public class PlayerSetupParser : IPlayerSetupParser
    {
        public IConsoleReader ThisReader { get; set; }
        public bool IsValidInput { get; set; }
        
        private string operationResult;

        public PlayerSetupParser(IConsoleReader thisReader)
        {
            ThisReader = thisReader;
            operationResult = string.Empty;
            IsValidInput = false;
        }

        public string SetPlayerName()
        {
            while (IsValidInput == false)
            {
                // get user input, validate and process it.
                ProcessUserNameInput();
            }
            // reset the property
            IsValidInput = false;
            return operationResult;
        }

        public void ProcessUserNameInput()
        {
            IsValidInput = true;
            string userInput = ThisReader.ReadConsole();
            if (string.IsNullOrWhiteSpace(userInput))
            {
                Console.WriteLine(Resources.getNameErrorMessage);
                IsValidInput = false;
            }
            else
            {
                operationResult = userInput;
                Console.WriteLine(Resources.playerNameSetConfirmation);
            }
        }

        public string PlaceNewShip()
        {
            while (IsValidInput == false)
            {
                // get the user input and validate it
                string userInput = ValidateUserShipInput(ThisReader.ReadConsole());
                // process the string and act on the result
                ProcessUserShipInput(userInput);
            }
            // reset the property
            IsValidInput = false;
            return operationResult;
        }

        public void ProcessUserShipInput(string userInput)
        {
            if (IsValidInput == false)
            { 
                Console.WriteLine(Resources.getPlaceErrorMessage); 
            }
            else
            {
                operationResult = userInput;
                Console.WriteLine(Resources.shipPositionsSetConfirmation);
            }
        }

        public string ValidateUserShipInput(string userInput)
        {
            // assume all is well
            IsValidInput = true;
            int x;
            int y;
            string[] stringSplits = userInput.Split(',');
            // now look for any invalid part
            if (stringSplits.Length != 3 
                || !(stringSplits[2] == "h" 
                || stringSplits[2] == "v") 
                || int.TryParse(stringSplits[1], out y) == false 
                || int.TryParse(stringSplits[0], out x) == false)
            {
                IsValidInput = false;
            }
            return userInput;
        }
    }
}