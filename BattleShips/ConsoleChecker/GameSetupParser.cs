﻿using BattleShips.BattleGround;
using BattleShips.Properties;
using BattleShips.Setup;
using System;
namespace BattleShips.ConsoleChecker
{
    /// <summary>
    /// This is a list of validation methods used to check user input during game setup.
    /// </summary>
    public class GameSetupParser : IGameSetupParser
    {
        public IConsoleReader ThisReader { get; set; }
        public int MinimumValue { get; set; }
        public string ErrorMsg { get; set; }
        public bool IsValidInput { get; set; }

        private GameMode thisGameMode;
        private Sea thisSea;
        private int operationResult;

        public GameSetupParser(IConsoleReader thisReader)
        {
            ThisReader = thisReader;
            IsValidInput = false;
        }

        public int SetNumberOfPlayers()
        {
            GetUserInt(mMinimumValue: 2, eErrorMsg: Resources.getPlayerNoErrorMessage);
            Console.WriteLine(Resources.playerNoSetConfirmation, operationResult);
            return operationResult;
        }

        public int SetNumberOfShips()
        {
            GetUserInt(mMinimumValue: 0, eErrorMsg: Resources.getShipNoErrorMessage);
            Console.WriteLine(Resources.shipNoSetConfirmation, operationResult);
            return operationResult;
        }

        private void GetUserInt(int mMinimumValue, string eErrorMsg)
        {
            MinimumValue = mMinimumValue;
            ErrorMsg = eErrorMsg;
            while (IsValidInput == false)
            {
                processUserInt();
            }
            IsValidInput = false;
        }

        public void processUserInt()
        {
            IsValidInput = true;
            int validItem;
            string input = ThisReader.ReadConsole();
            IsValidInput = int.TryParse(input, out validItem);
            if (IsValidInput == false || int.Parse(input) < MinimumValue)
            {
                Console.WriteLine(ErrorMsg);
                IsValidInput = false;
            }
            else { operationResult = validItem; }
        }

        public GameMode SelectGameMode()
        {
            var gameModeEnumLength = Enum.GetValues(typeof(GameMode)).Length;
            while (IsValidInput == false)
            {
                // get the user mode int, validate and process it.
                processUserModeInput(gameModeEnumLength);
            }
            IsValidInput = false;
            return thisGameMode;
        }

        public void processUserModeInput(int gameModeEnumLength)
        {
            int modeEntered;
            string userInput = ThisReader.ReadConsole();
            bool result = int.TryParse(userInput, out modeEntered);
            if (result == false 
                || int.Parse(userInput) < 0 
                || int.Parse(userInput) > gameModeEnumLength - 1)
            {
                Console.WriteLine(Resources.getModeErrorMessage);
                IsValidInput = false;
            }
            else
            {
                IsValidInput = true;
                // cast the int to its enum
                thisGameMode = (GameMode)modeEntered;
                Console.WriteLine(Resources.modeSetConfirmation, thisGameMode);
            }
        }

        public Sea SetSeaSize()
        {
            while (IsValidInput == false)
            {
                // split the user input and validate it
                string[] stringSplits = splitUserInput(ThisReader.ReadConsole());
                // process the strings and act on the result
                processInput(stringSplits);
            }
            // reset the property
            IsValidInput = false;
            return thisSea;
        }

        public string[] splitUserInput(string userInput)
        {
            IsValidInput = true;
            int validItem;
            string[] stringSplits = userInput.Split(',');
            foreach (var splitItem in stringSplits)
            {
                // try parsing each split part
                bool result = int.TryParse(splitItem, out validItem);
                if (result == false 
                    || int.Parse(splitItem) < 5 
                    || stringSplits.Length != 2)
                {
                    IsValidInput = false; 
                }
            }
            return stringSplits;
        }

        private void processInput(string[] stringSplits)
        {
            if (IsValidInput == false) 
            { 
                Console.WriteLine(Resources.getSeaErrorMessage); 
            }
            else
            {
                thisSea = new Sea(int.Parse(stringSplits[0]), int.Parse(stringSplits[1]));
                Console.WriteLine(Resources.seaSetConfirmation, stringSplits[0], stringSplits[1]);
            }
        }
    }
}