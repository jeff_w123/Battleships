﻿using BattleShips.BattleGround;
using BattleShips.Properties;
using System;

namespace BattleShips.ConsoleChecker
{
    /// <summary>
    /// This is a list of validation methods used to check user input during game play.
    /// </summary>
    public class GamePlayParser : IGamePlayParser
    {
        public IConsoleReader ThisReader { get; set; }
        public bool IsValidInput { get; set; }

        private Position firePosition;

        public GamePlayParser(IConsoleReader thisReader)
        {
            ThisReader = thisReader;
            IsValidInput = false;
        }

        public Position FireCommand()
        {
            while (IsValidInput == false)
            {
                // split the user input and validate it
                string[] stringSplits = splitUserInput(ThisReader.ReadConsole());
                // process the strings and act on the result
                processInput(stringSplits);
            }
            // reset the property
            IsValidInput = false;
            return firePosition;
        }

        public string[] splitUserInput(string userInput)
        {
            IsValidInput = true;
            int validItem;
            string[] stringSplits = userInput.Split(',');
            foreach (var splitItem in stringSplits)
            {
                // try parsing each split part
                bool result = int.TryParse(splitItem, out validItem);
                if (result == false 
                    || int.Parse(splitItem) < 0 
                    || stringSplits.Length != 2)
                { 
                    IsValidInput = false; 
                }
            }
            return stringSplits;
        }

        private void processInput(string[] stringSplits)
        {
            if (IsValidInput == false)
            { 
                Console.WriteLine(Resources.fireErrorMessage); 
            }
            else
            {
                firePosition = new Position(int.Parse(stringSplits[0]), int.Parse(stringSplits[1]));
            }
        }
    }
}