﻿namespace BattleShips.BattleGround

{
    /// <summary>
    /// This class represents a sea that uses integers to represent the x and y dimensions.
    /// </summary>
    public class Sea
    {
        #region properties
        public int SeaRow { get; set; }
        public int SeaColumn { get; set; }
        #endregion

        public Sea(int seaRow, int seaColumn)
        {
            SeaRow = seaRow;
            SeaColumn = seaColumn;
        }
        /// <summary>
        /// A method to check x and y coordinates to see if they lie within
        /// the sea.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public bool IsValidPosition(Position position)
        {
            return position.Row < SeaRow 
                && position.Column < SeaColumn 
                && position.Row >= 0 
                && position.Column >= 0;
        }
    }
}
