﻿using System.Collections.Generic;

namespace BattleShips
{
    public interface IGamePlay
    {
        List<Player> PlayerList { get; set; }
        void PlayGame();
    }
}
